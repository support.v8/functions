const getSum = (str1, str2) => {
  "use strict";
  if (!(typeof str1 === 'number' || typeof str1 === 'string' || typeof str2 === 'number' || typeof str2 === 'string'))
    return false;

  let val1 = Number(str1);
  let val2 = Number(str2);

  if (isNaN(val1) || isNaN(val2))
    return false;

  return (val1 + val2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  "use strict";
  const postByAuthor = listOfPosts.filter(post => post.author === authorName);
  let comments = 0;
  for (const post of listOfPosts)
    if (post.comments != undefined)
      comments += post.comments.filter(comment => comment.author === authorName).length;

  return `Post:${postByAuthor.length},comments:${comments}`;
};

const tickets=(people)=> {
  "use strict";

  let cash = {
    '25': 0,
    '50': 0,
    '100': 0
  }

  for (const bill of people)
    cash['' + bill]++;

  if (cash['50'] <= cash['25'])
    cash['25'] -= cash['50'];
  else
    return 'NO';

  while (cash['100'] > 0) {
    if (cash['50'] > 0 && cash['25'] > 0) {
      cash['50']--;
      cash['25']--;
    }
    else if (cash['25'] >= 3)
      cash['25'] -= 3;
    else
      return 'NO';
    cash['100']--;
  }

  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
